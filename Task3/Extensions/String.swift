//
//  String.swift
//  Task3
//
//  Created by Vadim Badretdinov on 29.10.2020.
//

import Foundation

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: "**\(self)**", comment: "")
    }
}
