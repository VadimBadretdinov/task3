//
//  LocLoginVC.swift
//  Task3
//
//  Created by Vadim Badretdinov on 29.10.2020.
//

import Foundation

enum LocLoginVC: String, Localizable {
    case errorLogin
    case errorEmpty
}
