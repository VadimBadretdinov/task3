//
//  Localization.swift
//  Task3
//
//  Created by Vadim Badretdinov on 29.10.2020.
//

import Foundation

protocol Localizable {
    var localized: String { get }
}

extension Localizable where Self: RawRepresentable, Self.RawValue == String {
    var localized: String {
        return rawValue.localized()
    }
}
