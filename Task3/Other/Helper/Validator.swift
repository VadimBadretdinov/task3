//
//  Validator.swift
//  Task3
//
//  Created by Vadim Badretdinov on 28.10.2020.
//

import Foundation

typealias Email = String
typealias Nickname = String

enum Validator {
    case email(Email)
    case nickname(Nickname)

    func validate() -> Bool {
        switch self {
        case let .email(mail):
            let emailRegEx = "[A-Za-z]{1}[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,}"
            let emailPred = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
            return emailPred.evaluate(with: mail)
        case let .nickname(nickname):
            let nickRegEx = "[A-Za-z]{1}[A-Z0-9a-z.-]{2,31}"
            let nickPred = NSPredicate(format: "SELF MATCHES %@", nickRegEx)
            return nickPred.evaluate(with: nickname)
        }
    }
}
