//
//  LoginVC.swift
//  Task3
//
//  Created by Vadim Badretdinov on 28.10.2020.
//

import UIKit

final class LoginVC: UIViewController {

    enum LoginState {
        case login, empty
    }

    //MARK: - Property

    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var errorLabel: UILabel!

    private var validationError: Bool = false {
        didSet {
            configure()
        }
    }

    private var state: LoginState = .login

    //MARK: - Override

    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        textField.layer.borderColor = UIColor.red.cgColor
    }

    //MARK: - Private

    private func configure() {
        if validationError {
            textField.layer.borderWidth = 1

            switch state {
            case .login:
                errorLabel.text = LocLoginVC.errorLogin.localized
            case .empty:
                errorLabel.text = LocLoginVC.errorEmpty.localized
            }
        } else {
            errorLabel.text = ""
            textField.layer.borderWidth = 0
        }
    }
}

//MARK: - UITextFieldDelegate

extension LoginVC: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        validationError = false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text, !text.isEmpty else {
            state = .empty
            validationError = true
            return
        }
        state = .login

        validationError = ![Validator.validate(.email(text))(),
                         Validator.validate(.nickname(text))()].contains(true)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
