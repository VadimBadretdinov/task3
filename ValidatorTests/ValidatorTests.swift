//
//  ValidatorTests.swift
//  ValidatorTests
//
//  Created by Vadim Badretdinov on 29.10.2020.
//

import XCTest
@testable import Task3

class ValidatorTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testValidateEmail() {
        XCTAssertTrue(Validator.validate(.email("asdwdwa@mail.ru"))())
    }

    func testFistsNumberEmail() {
        XCTAssertFalse(Validator.validate(.email("1asdwdwa@mail.ru"))())
    }

    func testFistsDotEmail() {
        XCTAssertFalse(Validator.validate(.email(".asdwdwa@mail.ru"))())
    }

    func testFistsMinusEmail() {
        XCTAssertFalse(Validator.validate(.email("-asdwdwa@mail.ru"))())
    }

    func testRussianEmail() {
        XCTAssertFalse(Validator.validate(.email("иос@иос.ru"))())
    }

    func testFirstNumberNickname() {
        XCTAssertFalse(Validator.validate(.nickname("1asdwdwa"))())
    }

    func testFirstDotNickname() {
        XCTAssertFalse(Validator.validate(.nickname(".asdwdwa"))())
    }

    func testFirstMinusNickname() {
        XCTAssertFalse(Validator.validate(.nickname(".asdwdwa"))())
    }

    func testEmptyNickname() {
        XCTAssertFalse(Validator.validate(.nickname(""))())
    }

    func testLessSymbolsNickname() {
        XCTAssertFalse(Validator.validate(.nickname("aa"))())
    }

    func testRussianNickname() {
        XCTAssertFalse(Validator.validate(.nickname("Вадим"))())
    }

    func testMinLenghtNick() {
        XCTAssertFalse(Validator.validate(.nickname("aa"))())
    }
    
    func testMaxLenghtNick() {
        let str = String.init(repeating: "a", count: 35)
        XCTAssertFalse(Validator.validate(.nickname(str))())
    }
}
